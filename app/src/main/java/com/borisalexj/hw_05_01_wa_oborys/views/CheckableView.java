package com.borisalexj.hw_05_01_wa_oborys.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Checkable;

import com.borisalexj.hw_05_01_wa_oborys.R;
import com.borisalexj.hw_05_01_wa_oborys.adapters.SingleChoiceRecyclerViewAdapter;

import static android.R.attr.x;

public class CheckableView extends View implements Checkable {
    private static final String TAG = CheckableView.class.getSimpleName();

    public interface OnCheckedChangeListener {
        void onCheckedChanged(boolean isChecked);
    }

    private static final int[] CHECKED_STATE_SET = {android.R.attr.state_checked};

    private boolean mChecked = false;

    private OnCheckedChangeListener mOnCheckedChangeListener;

    private Paint mTextPaint = new Paint();

    private String mValue="test";

    private float mHalfW=0;

    private float mHalfH=0;

    private boolean isCheckable=false;

    private OnClickListener mExternalClickListener;
    final private OnClickListener mInternalClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Log.d(TAG, "onClick: ");
            if (isCheckable) {
                toggle();
            }
            if (mExternalClickListener != null){
                mExternalClickListener.onClick(v);
            }
        }
    };

    public CheckableView(Context context) {
        this(context, null);
        Log.d(TAG, "CheckableView: ");
    }

    public CheckableView(Context context, AttributeSet attrs) {
        this(context, attrs, -1);
        Log.d(TAG, "CheckableView: ");
    }

    public CheckableView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        super.setOnClickListener(mInternalClickListener);
        Log.d(TAG, "CheckableView: ");
        mTextPaint = new Paint();
        mTextPaint.setTextSize(R.dimen.sp14);
        mTextPaint.setTextAlign(Paint.Align.CENTER);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Log.d(TAG, "onDraw: ");
        canvas.drawText(mValue,mHalfW,mHalfH,mTextPaint);
    }

    @Override
    public boolean isChecked() {
        Log.d(TAG, "isChecked: ");
        return mChecked;
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        Log.d(TAG, "onSizeChanged: ");
        mHalfW = w/2.0f;
        mHalfH = w/2.0f;
    }

    public void setChecked(boolean b) {
        Log.d(TAG, "setChecked: ");
        if (b != mChecked)
            mChecked = b;
        refreshDrawableState();
        if (mOnCheckedChangeListener != null){
            mOnCheckedChangeListener.onCheckedChanged(mChecked);
        }
    }


    @Override
    public void toggle() {
        Log.d(TAG, "toggle: ");
        setChecked(!mChecked);
    }

    @Override
    protected int[] onCreateDrawableState(int extraSpace) {
        Log.d(TAG, "onCreateDrawableState: ");
        final int[] drawableState = super.onCreateDrawableState(extraSpace +1);
        if (isChecked()){
            mergeDrawableStates(drawableState, CHECKED_STATE_SET);
        }
        return drawableState;
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener onCheckedChangeListener) {
        Log.d(TAG, "setOnCheckedChangeListener: ");
        mOnCheckedChangeListener = onCheckedChangeListener;
    }

    public void setValue(String value) {
        Log.d(TAG, "setValue: ");
        mValue = value;
    }


    public void setCheckable(boolean checkable) {
        Log.d(TAG, "setCheckable: ");
        isCheckable = checkable;
    }

    @Override
    public void setOnClickListener(OnClickListener listener) {
        Log.d(TAG, "setOnClickListener: ");
        mExternalClickListener = listener;
    }

    //    public CheckableView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
//        super(context, attrs, defStyleAttr, defStyleRes);
//    }

}
