package com.borisalexj.hw_05_01_wa_oborys.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.CheckedTextView;

import com.borisalexj.hw_05_01_wa_oborys.R;
import com.borisalexj.hw_05_01_wa_oborys.adapters.viewholders.CheckableViewHolder;
import com.borisalexj.hw_05_01_wa_oborys.views.CheckableView;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;


public class SingleChoiceRecyclerViewAdapter extends RecyclerView.Adapter<CheckableViewHolder> {

    private static final String TAG = SingleChoiceRecyclerViewAdapter.class.getSimpleName();


    private static final String PARAM_ITEMS_ARRAY_LIST = "PARAM_ITEMS_ARRAY_LIST";

    private final SparseBooleanArray mSelectedItemsArray;
    private final List<String> mItemsArrayList;
    private final LayoutInflater mLayoutInflater;
    private ChoiceListener mExternalChoiceListener;
    private boolean isActionModeActivated;

    public interface ChoiceListener {

        void updateSelectedItemsCount(int count);

        void updateTotalItemsCount(int count);

    }

    private ChoiceListener mInternalChoiceListener = new ChoiceListener() {
        @Override
        public void updateSelectedItemsCount(int count) {
            Log.d(TAG, "updateSelectedItemsCount: ");
            if (mExternalChoiceListener != null) {
                mExternalChoiceListener.updateSelectedItemsCount(count);
            }
        }

        @Override
        public void updateTotalItemsCount(int count) {
            Log.d(TAG, "updateTotalItemsCount: ");
            if (mExternalChoiceListener != null) {
                mExternalChoiceListener.updateTotalItemsCount(count);
            }
        }
    };

    private CheckableViewHolder.Callback mViewHolderCallBack = new CheckableViewHolder.Callback() {
        public void check(int id, boolean isChecked) {
            Log.d(TAG, "check: ");

            if (isChecked) {
                mSelectedItemsArray.put(id, true);
            } else {
                mSelectedItemsArray.delete(id);
            }
            mInternalChoiceListener.updateSelectedItemsCount(mSelectedItemsArray.size());
        }
    };


    public SingleChoiceRecyclerViewAdapter(Context context) {
        Log.d(TAG, "SingleChoiceRecyclerViewAdapter: ");
        this.mSelectedItemsArray = new SparseBooleanArray();
        this.mItemsArrayList = new ArrayList<String>();
        this.mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public CheckableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder: ");
//        CheckableView view = (CheckableView) mLayoutInflater.inflate(R.layout.checkable_recycler_view_item_layout, parent, false);
        CheckedTextView view = (CheckedTextView) mLayoutInflater.inflate(android.R.layout.simple_list_item_checked, parent, false);
//        return new CheckableViewHolder(mViewHolderCallBack, view);
        return new CheckableViewHolder(mViewHolderCallBack, view);
    }

    @Override
    public void onBindViewHolder(CheckableViewHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: ");
        holder.setValue(mItemsArrayList.get(position));
        holder.setCheckable(isActionModeActivated);
        holder.setChecked(mSelectedItemsArray.get(position, false));
    }

    @Override
    public int getItemCount() {
        Log.d(TAG, "getItemCount: ");
        return mItemsArrayList.size();
    }

    @Override
    public long getItemId(int position) {
        Log.d(TAG, "getItemId: ");
        return super.getItemId(position);
    }

    public void setActionMode(boolean isEnabled) {
        Log.d(TAG, "setActionMode: ");
        isActionModeActivated = isEnabled;
        notifyDataSetChanged();
        clearSelection();
    }


    public void addItem(String item) {
        Log.d(TAG, "addItem: ");
        mItemsArrayList.add(item);
        mInternalChoiceListener.updateTotalItemsCount(mItemsArrayList.size());
        notifyItemInserted(mItemsArrayList.size());
    }

    public void clearSelection() {
        Log.d(TAG, "clearSelection: ");
        mSelectedItemsArray.clear();
        refreshCounters();
        notifyDataSetChanged();
    }

    public void refreshCounters() {
        Log.d(TAG, "refreshCounters: ");
        mInternalChoiceListener.updateSelectedItemsCount(mSelectedItemsArray.size());
        mInternalChoiceListener.updateTotalItemsCount(mItemsArrayList.size());
    }

    public void selectAll() {
        Log.d(TAG, "selectAll: ");
        int size = mItemsArrayList.size();
        for (int i = 0; i < size; i++) {
            mSelectedItemsArray.put(i, true);
        }
        refreshCounters();
        notifyDataSetChanged();
    }

    public void removeSelectedItems() {
        Log.d(TAG, "removeSelectedItems: ");
        int index = 0;
        ListIterator<String> iterator = mItemsArrayList.listIterator();
        while (iterator.hasNext()) {
            iterator.next();
            if (mSelectedItemsArray.indexOfKey(index) >= 0) {
                iterator.remove();
            }
            index++;
        }
        mSelectedItemsArray.clear();
        refreshCounters();
        notifyDataSetChanged();
    }

    public void setChoiceListener(ChoiceListener choiceListener) {
        Log.d(TAG, "setChoiceListener: ");
        this.mExternalChoiceListener = choiceListener;
    }

    public void onSaveInstanceState(@NonNull Bundle outState) {
        Log.d(TAG, "onSaveInstanceState: ");
        outState.putStringArrayList(PARAM_ITEMS_ARRAY_LIST, (ArrayList<String>) mItemsArrayList);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onRestoreInstanceState: ");
        if (savedInstanceState != null) {
            List<String> savedStateList = savedInstanceState.getStringArrayList(PARAM_ITEMS_ARRAY_LIST);
            if (savedStateList != null && !savedStateList.isEmpty()) {
                mItemsArrayList.addAll(savedStateList);
            }
        }
        refreshCounters();
    }
}
