package com.borisalexj.hw_05_01_wa_oborys.adapters.viewholders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Checkable;
import android.widget.CheckedTextView;

import com.borisalexj.hw_05_01_wa_oborys.views.CheckableView;

public class CheckableViewHolder extends RecyclerView.ViewHolder {
    private static final String TAG = CheckableViewHolder.class.getSimpleName();

    public interface Callback {

        void check(int id, boolean isChecked);

    }

//    private CheckableView mView;
        private CheckedTextView mView;
    private Callback mExternalCallBack;

    private CheckableView.OnCheckedChangeListener mInternalCheckListener = new CheckableView.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(boolean isChecked) {
            Log.d(TAG, "onCheckedChanged: ");
            if (mExternalCallBack != null) {
                mExternalCallBack.check(getAdapterPosition(), isChecked);
            }
        }
    };


    public CheckableViewHolder(@NonNull Callback callback, @NonNull CheckedTextView itemView) {
        super(itemView);
        Log.d(TAG, "CheckableViewHolder: ");
        mExternalCallBack = callback;
        mView = itemView;
        mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Checkable)v).toggle();
                if (mExternalCallBack!=null){
                    mExternalCallBack.check(getAdapterPosition(),((Checkable)v).isChecked());
                }
            }
        });
//        mView.setOnCheckedChangeListener(mInternalCheckListener);
    }

    public void setValue(String value) {
        Log.d(TAG, "setValue: ");
//        mView.setValue(value);
        mView.setText(value);
    }

    public void setChecked(boolean isChecked) {
        Log.d(TAG, "setChecked: ");
        mView.setChecked(isChecked);
    }

    public void setCheckable(boolean isCheckable) {
        Log.d(TAG, "setCheckable: ");
//        mView.setCheckable(isCheckable);
        mView.setChecked(isCheckable);
    }
}
