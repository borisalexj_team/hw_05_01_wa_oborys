package com.borisalexj.hw_05_01_wa_oborys.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.borisalexj.hw_05_01_wa_oborys.R;
import com.borisalexj.hw_05_01_wa_oborys.adapters.SingleChoiceRecyclerViewAdapter;

/**
 * Created by user on 4/9/2017.
 */

public class RecyclerViewFragment extends Fragment {

    private static final String TAG = RecyclerViewFragment.class.getSimpleName();
    private static final int COLUMNS = 2;

    private RecyclerView mRecyclerView;
    private SingleChoiceRecyclerViewAdapter mAdapter;
    private ActionMode mActionMode;

    private SingleChoiceRecyclerViewAdapter.ChoiceListener mChoiceListener = new SingleChoiceRecyclerViewAdapter.ChoiceListener() {

        @Override
        public void updateSelectedItemsCount(int count) {
            Log.d(TAG, "updateSelectedItemsCount: " + count);
            if (mActionMode != null) {
                mActionMode.setTitle(getString(R.string.action_mode_title,String.valueOf(count)));
        }

        }

        @Override
        public void updateTotalItemsCount(int count) {
            Log.d(TAG, "updateTotalItemsCount: ");
            if (mActionMode != null) {
                mActionMode.setTitle(getString(R.string.action_mode_title_total,String.valueOf(count)));
            }

        }
    };

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            Log.d(TAG, "onCreateActionMode: ");
            mode.getMenuInflater().inflate(R.menu.action_mode_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            Log.d(TAG, "onPrepareActionMode: ");
            mAdapter.setActionMode(true);
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            Log.d(TAG, "onActionItemClicked: ");
            boolean result = true;
            switch (item.getItemId()) {
                case R.id.clear_selection_menu_item: {
                    mAdapter.clearSelection();
                    break;
                }
                case R.id.select_all_menu_item: {
                    mAdapter.selectAll();
                    break;
                }
                case R.id.remove_menu_item: {
                    mAdapter.removeSelectedItems();
                    mode.finish();
                    break;
                }
                default: {
                    result = false;
                }
            }

            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            Log.d(TAG, "onDestroyActionMode: ");
            mActionMode = null;
            mAdapter.setActionMode(false);
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate: ");
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    GridLayoutManager gridLayoutManager;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated: ");
        gridLayoutManager = new GridLayoutManager(getContext(), COLUMNS, GridLayoutManager.VERTICAL, false);

        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return (position +1) %3 == 0 ? 2: 1;
            }
        });
        mAdapter = new SingleChoiceRecyclerViewAdapter(getContext());
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setChoiceListener(mChoiceListener);
        mAdapter.onRestoreInstanceState(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        Log.d(TAG, "onCreateOptionsMenu: ");
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "onOptionsItemSelected: ");
        boolean result = true;
        switch (item.getItemId()){
            case R.id.add_menu_item:{
                mAdapter.addItem(String.valueOf(System.currentTimeMillis()));
                break;
            }
            case R.id.edit_menu_item:{
                mActionMode = ((AppCompatActivity) getActivity())
                        .startSupportActionMode(mActionModeCallback);
            }
            default:{
                result = super.onOptionsItemSelected(item);
            }
        }
        return result;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState: ");
        mAdapter.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }
}
